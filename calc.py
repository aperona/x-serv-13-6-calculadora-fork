

def sumar(a, b):
    return a + b


def restar(a, b):
    return a - b


def calculadora():
    suma = sumar(1, 2)
    print("Sumar 1 y 2: " + str(suma))

    print("---------------------------")

    suma = sumar(3, 4)
    print("Sumar 3 y 4: " + str(suma))

    print("---------------------------")

    resta = restar(6, 5)
    print("Restar 5 a 6: " + str(resta))

    print("---------------------------")

    resta = restar(8, 7)
    print("Restar 7 a 8: " + str(resta))


calculadora()
